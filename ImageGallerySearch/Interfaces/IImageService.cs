﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ImageGallerySearch.Models;

namespace ImageGallerySearch.Interfaces
{
    public interface IImageService
    {
        Task LoadImagesAsync();

        Task<List<Picture>> SearchAsync(string pattern);
    }
}
