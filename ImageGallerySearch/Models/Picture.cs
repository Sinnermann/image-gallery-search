﻿using Newtonsoft.Json;

namespace ImageGallerySearch.Models
{
    public class Picture
    {
        public string Id { get; set; }

        public string Author { get; set; }

        public string Camera { get; set; }

        public string Tags { get; set; }

        [JsonProperty("full_picture")]
        public string FullPictureUrl { get; set; }

        [JsonProperty("cropped_picture")]
        public string CroppedPictureUrl { get; set; }
    }
}
