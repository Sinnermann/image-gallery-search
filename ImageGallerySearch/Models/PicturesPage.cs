﻿using System.Collections.Generic;

namespace ImageGallerySearch.Models
{
    public class PicturesPage
    {
        public List<PictureLight> Pictures { get; set; }

        public int Page { get; set; }

        public int PageCount { get; set; }

        public bool HasMore { get; set; }
    }
}
