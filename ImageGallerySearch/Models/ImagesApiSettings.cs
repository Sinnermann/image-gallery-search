﻿namespace ImageGallerySearch.Models
{
    public class ImagesApiSettings
    {
        public string BaseUrl { get; set; }

        public string ApiKey { get; set; }

        public int CacheExpiresInMinutes { get; set; }
    }
}
