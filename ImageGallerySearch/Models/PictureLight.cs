﻿using Newtonsoft.Json;

namespace ImageGallerySearch.Models
{
    public class PictureLight
    {
        public string Id { get; set; }

        [JsonProperty("cropped_picture")]
        public string CroppedPictureUrl { get; set; }
    }
}
