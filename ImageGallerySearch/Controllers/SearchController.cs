﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ImageGallerySearch.Interfaces;
using ImageGallerySearch.Models;
using Microsoft.AspNetCore.Mvc;

namespace ImageGallerySearch.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SearchController : ControllerBase
    {
        private readonly IImageService _imageService;

        public SearchController(IImageService imageService)
        {
            _imageService = imageService;
        }

        [HttpGet("{pattern}")]
        public async Task<IEnumerable<Picture>> GetAsync(string pattern)
        {
            return await _imageService.SearchAsync(pattern);
        }
    }
}
