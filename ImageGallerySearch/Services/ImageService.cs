﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ImageGallerySearch.Interfaces;
using ImageGallerySearch.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ImageGallerySearch.Services
{
    public class ImageService : IImageService
    {
        private readonly IMemoryCache _cache;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly ImagesApiSettings _apiSettings;

        public ImageService(IMemoryCache cache,
            IHttpClientFactory httpClientFactory,
            IOptions<ImagesApiSettings> apiSettings)
        {
            _cache = cache;
            _httpClientFactory = httpClientFactory;
            _apiSettings = apiSettings.Value;
        }

        public async Task LoadImagesAsync()
        {
            await GetPicturesAsync();
        }

        public async Task<List<Picture>> SearchAsync(string pattern)
        {
            var pictures = await GetPicturesAsync();

            //TODO: here should be implemented more flexible method to search pictures
            return pictures
                .Where(p => (p.Author != null && p.Author.Contains(pattern)) ||
                    (p.Camera != null && p.Camera.Contains(pattern)) ||
                    (p.Tags != null && p.Tags.Contains(pattern)))
                .ToList();
        }

        private async Task<List<Picture>> GetPicturesAsync()
        {
            return await _cache.GetOrCreateAsync("pictures", entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(_apiSettings.CacheExpiresInMinutes);
                return FetchPicturesAsync();
            });
        }

        private async Task<string> GetBearerTokenAsync()
        {
            return await _cache.GetOrCreateAsync("token", entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(_apiSettings.CacheExpiresInMinutes);
                return FetchBearerToken();
            });
        }

        private async Task<List<Picture>> FetchPicturesAsync()
        {
            var token = await GetBearerTokenAsync();

            var pictureIds = await FetchPictureIdsAsync(token);

            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");

            var pictures = new List<Picture>();

            foreach (var pictureId in pictureIds)
            {
                var response = await client.GetAsync($"{_apiSettings.BaseUrl}/images/{pictureId}");
                var content = await response.Content.ReadAsStringAsync();
                var picture = JsonConvert.DeserializeObject<Picture>(content);
                pictures.Add(picture);
            }

            return pictures;
        }

        private async Task<IEnumerable<string>> FetchPictureIdsAsync(string token)
        {
            var client = _httpClientFactory.CreateClient();
            client.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");

            PicturesPage picturesPagedResult = new PicturesPage();
            List<PictureLight> pictureLinks = new List<PictureLight>();
            do
            {
                var response = await client.GetAsync($"{_apiSettings.BaseUrl}/images?page={picturesPagedResult.Page + 1}");
                var content = await response.Content.ReadAsStringAsync();
                picturesPagedResult = JsonConvert.DeserializeObject<PicturesPage>(content);
                pictureLinks.AddRange(picturesPagedResult.Pictures);

            } while (picturesPagedResult.HasMore);

            return pictureLinks.Select(p => p.Id).ToArray();
        }

        private async Task<string> FetchBearerToken()
        {
            var authRequest = new HttpRequestMessage(HttpMethod.Post, $"{_apiSettings.BaseUrl}/auth");
            var authContent = JsonConvert.SerializeObject(
                new AuthRequest { ApiKey = _apiSettings.ApiKey },
                new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            authRequest.Content = new StringContent(authContent, Encoding.UTF8, "application/json");

            var client = _httpClientFactory.CreateClient();

            var response = await client.SendAsync(authRequest);
            var content = await response.Content.ReadAsStringAsync();

            var authResult = JsonConvert.DeserializeObject<AuthResponse>(content);

            if (!authResult.Auth)
            {
                throw new Exception($"Auth request to {_apiSettings.BaseUrl}/auth was failed. Check Api Key");
            }

            return authResult.Token;
        }
    }
}
