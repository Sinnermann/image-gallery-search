﻿using System.Threading;
using System.Threading.Tasks;
using ImageGallerySearch.Interfaces;
using Microsoft.Extensions.Hosting;

namespace ImageGallerySearch.Services
{
    public class ImageLoader : IHostedService
    {
        private readonly IImageService _imageService;

        public ImageLoader(IImageService imageService)
        {
            _imageService = imageService;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await _imageService.LoadImagesAsync();
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    }
}
